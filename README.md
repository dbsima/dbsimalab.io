# dbsima.gitlab.io

This is the repo behing [dbsima.gitlab.io](http://dbsima.gitlab.io/).

## Getting started

### Install Global Dependencies
  * [Node.js](http://nodejs.org)
  * [bower](http://bower.io): `[sudo] npm install bower -g`
  * [grunt.js](http://gruntjs.com); `[sudo] npm install -g grunt-cli`

### Install Local Dependencies
  * Clone the repo]
  * cd to project folder
  * run `[sudo] npm install` (first time users)
  * run `grunt` (to watch and compile Less files)

### Content
```
dbsima.gitlab.io/
├── .gitignore
├── .gitlab-ci.yml
├── Gruntfile.js
├── README.md
├── bower.json
├── package-lock.json
├── package.json
└── public
    ├── blog.html
    ├── css
    │   └── skeleton.css
    ├── images
    │   └── favicon.png
    ├── index.html
    └── less
        └── skeleton.less
```

## License

All parts of Skeleton-Less are free to use and abuse under the [open-source MIT license](http://opensource.org/licenses/mit-license.php).

## Acknowledgement

Skeleton was created by [Dave Gamache](https://twitter.com/dhg) for a better web.
Skeleton-Less was created by [Seth Coelen](http://sethcoelen.com) for a better Skeleton.
