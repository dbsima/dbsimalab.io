# -*- coding: utf-8 -*-
from jinja2 import Environment, FileSystemLoader
from utils import get_html_files

PATH_TO_POSTS = '_posts/'
PATH_TO_PUBLIC = 'public/'

env = Environment(loader=FileSystemLoader(PATH_TO_POSTS))

for f in get_html_files(PATH_TO_POSTS):
    if f == '_template.html':
        continue

    template = env.get_template(f)
    output = template.render()

    dest = PATH_TO_PUBLIC + f.replace('_', '')

    with open(dest, 'w+') as fh:
        fh.write(output)
        fh.close()
