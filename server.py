# -*- coding: utf-8 -*-

import jinja2
from flask import Flask, request, render_template, views, send_from_directory
from utils import get_html_files

# set the project root directory as the static folder, you can set others.
app = Flask(__name__)

PATH_TO_POSTS = '_posts'

# Custom template folder
my_loader = jinja2.ChoiceLoader([
    app.jinja_loader,
    jinja2.FileSystemLoader(PATH_TO_POSTS),
])
app.jinja_loader = my_loader


class GenericView(views.View):
    methods = ['GET']

    def dispatch_request(self, name):
        if name in ['index']:
            name = '_' + name
        try:
            return render_template(name + '.html')
        except jinja2.exceptions.TemplateNotFound:
            return 'This page does not exist', 404


for f in get_html_files(PATH_TO_POSTS):
    # Dynamically generate the routes
    if f == '_template.html':
        continue
    if f.startswith('_'):
        f = f.replace('_', '')
    app.add_url_rule('/<name>', view_func=GenericView.as_view(f))


@app.route('/css/<path:path>')
def send_css(path):
    # Serve the static CSS file
    return send_from_directory('public/css', path)


@app.route('/images/<path:path>')
def send_images(path):
    # Serve the static images file
    return send_from_directory('public/images', path)


if __name__ == "__main__":
    app.run(debug=True, use_debugger=True, use_reloader=True)
