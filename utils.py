# -*- coding: utf-8 -*-
from os import listdir
from os.path import isfile, join


def get_html_files(path):
    """ Return the list of HTML files from <path>
    """
    files = []
    for f in listdir(path):
        if not isfile(join(path, f)):
            continue
        if not f.endswith('.html'):
            continue
        files.append(f)
    return files



